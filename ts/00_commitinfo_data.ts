/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartstream',
  version: '2.0.4',
  description: 'simplifies access to node streams'
}
