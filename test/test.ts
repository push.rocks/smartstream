import * as smartfile from '@push.rocks/smartfile';
import { expect, tap } from '@push.rocks/tapbundle';

import * as smartstream from '../ts/smartstream.classes.streamwrapper.js';

let testSmartstream: smartstream.StreamWrapper;
tap.test('should combine a stream', async () => {
  testSmartstream = new smartstream.StreamWrapper([
    smartfile.fsStream.createReadStream('./test/assets/test.md'),
    smartfile.fsStream.createWriteStream('./test/assets/testCopy.md'),
  ]);
  await testSmartstream.run();
});

tap.start();
